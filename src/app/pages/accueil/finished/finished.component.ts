import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatTableDataSource} from '@angular/material';

@Component({
  selector: 'app-finished',
  templateUrl: './finished.component.html',
  styleUrls: ['./finished.component.css']
})
export class FinishedComponent implements OnInit {
  displayedColumns: string[] = ['Date','IdCartographie','Chantier', 'Action1','Action2'];
  dataSource = new MatTableDataSource<Cartographie>(ELEMENT_DATA);

  @ViewChild(MatPaginator) paginator: MatPaginator;
  
  constructor() {
    
   }

  ngOnInit() {
    this.dataSource.paginator = this.paginator;
  }
}
export interface Cartographie {
  Date : string, 
  IdCartographie: string,
  Chantier : string, 
  Action1:string,
  Action2:string
}
const ELEMENT_DATA: Cartographie[] = [
  {Date: '1', IdCartographie: 'Id1', Chantier: 'chant1', Action1: 'Modifier',Action2: 'Télécharger'},
  {Date: '1', IdCartographie: 'Id1', Chantier: 'chant1', Action1: 'Modifier',Action2: 'Télécharger'},
  {Date: '1', IdCartographie: 'Id1', Chantier: 'chant1', Action1: 'Modifier',Action2: 'Télécharger'},
  {Date: '1', IdCartographie: 'Id1', Chantier: 'chant1', Action1: 'Modifier',Action2: 'Télécharger'},
  {Date: '1', IdCartographie: 'Id1', Chantier: 'chant1', Action1: 'Modifier',Action2: 'Télécharger'},
  {Date: '1', IdCartographie: 'Id1', Chantier: 'chant1', Action1: 'Modifier',Action2: 'Télécharger'},
  {Date: '2', IdCartographie: 'Id2', Chantier: 'chant2', Action1: 'Modifier',Action2: 'Télécharger'},
  {Date: '3', IdCartographie: 'Id3', Chantier: 'chant3', Action1: 'Modifier',Action2: 'Télécharger'}
]; 
