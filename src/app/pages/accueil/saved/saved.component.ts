import { Component, OnInit, ViewChild} from '@angular/core';
import { MatPaginator, MatTableDataSource} from '@angular/material';
import { Router } from '@angular/router'

import { Cartographie } from '../../../services/cartographie.model';
import { SampleObject} from '../../../services/cartographie.service';
import { CartographieService } from '../../../services/cartographie.service';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-saved',
  templateUrl: './saved.component.html',
  styleUrls: ['./saved.component.css']
})
export class SavedComponent implements OnInit {
  displayedColumns: string[] = ['date','identifiant','chantier'];
  dataSource = new MatTableDataSource<SampleObject>();
  cartographies: SampleObject[];
  
  paginator: MatPaginator;
  
  constructor(private router: Router, private cartographieService: CartographieService) {  
  }
  
  ngOnInit() { 
    this.getData();
    console.log(this.cartographies)
    this.dataSource.paginator = this.paginator;  
  }

  getData():void{
    this.cartographieService.getSampleObject().subscribe( 
      data =>{
        this.cartographies = data;
        console.log(this.cartographies);
      },
      error => console.log("Error :: " + error)
      //this.dataSource= new MatTableDataSource(this.cartographies);
      //console.log(this.cartographies);
      // Problem : changes are local in subscribe     
    );  
    
    //return this.cartographies;
  };
  
}
