import { Component, OnInit } from '@angular/core';
import { Content } from '@angular/compiler/src/render3/r3_ast';
import {ViewEncapsulation}  from '@angular/core'
import { from } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HeaderComponent implements OnInit {

  text : string 
  Content;
  constructor() { 
    this.text = 'Remind File Mapping System';
  }

  ngOnInit() {
  }

}
