export class Cartographie {
    id : Number;
    email: string;
    entity: string;
    codeChantier: string;
    sourceSystem: string;
    planClassementInit: string;
    lastPath: string;
    association:string[];

    constructor() { }
  }