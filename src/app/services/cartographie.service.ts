import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {Observable } from 'rxjs';
import {Http,Response} from '@angular/http';
import { Cartographie } from '../services/cartographie.model';

import 'rxjs/add/operator/map';
import 'rxjs/Rx';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

export class SampleObject{
   date : string;
	 identifiant : string;
	 chantier : string ;
}

@Injectable({
  providedIn: 'root'
})
export class CartographieService {
  private savedUrl ='/api/saved';
  private finishedUrl ='/api/finished';

  private sampleUrl = '/api';

  constructor(private http:Http) { }

  public getCartographieSaved(){
    return this.http.get(this.savedUrl);
  }
  public getCartographiesFinished(){
    return this.http.get(this.finishedUrl);
  }


  public getSampleObject(): Observable<SampleObject[]>{
    return this.http.get(this.sampleUrl)
      .map((response: Response) => {
        return <SampleObject[]>response.json();
     })
     .catch(this.handleError);
  }

  private handleError(error: Response) {
    return Observable.throw(error.statusText);
}
}

