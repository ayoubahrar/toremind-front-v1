import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MatPaginatorModule} from '@angular/material';
import { MatTableModule} from '@angular/material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CartographieService } from '../app/services/cartographie.service';
import { HttpModule} from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { PagesComponent } from './pages/pages.component';
import { AccueilComponent } from './pages/accueil/accueil.component';
import { SavedComponent } from './pages/accueil/saved/saved.component';
import { FinishedComponent } from './pages/accueil/finished/finished.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PagesComponent,
    AccueilComponent,
    SavedComponent,
    FinishedComponent
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatPaginatorModule,
    MatTableModule,
    NoopAnimationsModule,
    BrowserAnimationsModule,
    HttpClientModule,
    HttpModule
  ],
  providers: [CartographieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
