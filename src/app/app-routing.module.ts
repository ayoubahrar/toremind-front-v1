import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FinishedComponent } from './pages/accueil/finished/finished.component';
import { SavedComponent } from './pages/accueil/saved/saved.component';

const routes: Routes = [
  {path: 'cartographies/finished',component:FinishedComponent},
  {path: 'cartographies/saved',component:SavedComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
